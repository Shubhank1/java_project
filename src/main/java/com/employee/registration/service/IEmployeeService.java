package com.employee.registration.service;
import java.util.List;

import com.employee.registration.dto.Employee;
import com.employee.registration.dto.RequestBean;
import com.employee.registration.dto.ResponseDto;


public interface IEmployeeService {
	
	
	public String saveAllEmployees(List<Employee> employeeList);
	public ResponseDto fetchEmployeeDetails(Long id);
	public ResponseDto updateEmployeeDetails(Long employeeid, Employee employee);
}
