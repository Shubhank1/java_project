package com.employee.registration.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.employee.registration.dao.IEmployeeDao;
import com.employee.registration.dto.Email;
import com.employee.registration.dto.Employee;
import com.employee.registration.dto.RequestBean;
import com.employee.registration.dto.ResponseDto;
import com.employee.registration.model.EmailRegistration;
import com.employee.registration.model.EmployeeRegistrationModel;

@Service
public class EmployeeService implements IEmployeeService{

	
	@Autowired
	private IEmployeeDao iEmployeeDao;
	
	@Override
	public String saveAllEmployees(List<Employee> employeeList) {
		// TODO Auto-generated method stub
		
		for (Employee employee : employeeList) {
			employeeRegistration(employee);
		}
		System.out.println("from for loop of dao class");
		return null;
	}
	public void employeeRegistration(Employee empBean) {
		EmployeeRegistrationModel empModel = new EmployeeRegistrationModel();
		
		empModel.setId(3L);
		empModel.setTitle(empBean.getTitle());
		empModel.setFirst_Name(empBean.getFirst_Name());
		empModel.setMiddle_Name(empBean.getMiddle_Name());
		empModel.setLast_Name(empBean.getLast_Name());
		empModel.setAge(empBean.getAge());
		empModel.setDesignation(empBean.getDesignation());
		empModel.setIsActive(empBean.getIsActive());
		empModel.setPrimary_Phone(empBean.getPrimary_Phone());
		empModel.setEmployeeType(empBean.getEmployeeType());
System.out.println("from before service section");
		
		empModel = iEmployeeDao.save(empModel);
		System.out.println("from after service section" + empModel);

	}
	@Override
	public ResponseDto fetchEmployeeDetails(Long id) {
		// TODO Auto-generated method stub
		ResponseDto response = new ResponseDto();
		EmployeeRegistrationModel model = iEmployeeDao.findOne(id);
		if(model!=null)
		{
			response.setMessage("Data fetched Successfully");
			response.setStatus("SUCCESS");
			Employee emp = new Employee();
			emp.setAge(model.getAge());
			emp.setDesignation(model.getDesignation());
			emp.setEmployeeType(model.getEmployeeType());
			emp.setFirst_Name(model.getFirst_Name());
			emp.setLast_Name(model.getLast_Name());
			
			response.setEmployee(emp);
			
		}
		else {
			response.setMessage("ID not found");
			response.setStatus("FAILED");
		}
		return response;
	}
	@Override
	public ResponseDto updateEmployeeDetails(Long employeeid, Employee employee) {
		// TODO Auto-generated method stub
		ResponseDto response = new ResponseDto();
		EmployeeRegistrationModel model = iEmployeeDao.findOne(employeeid);
		if(model != null && employee != null)
		{
			model.setFirst_Name(employee.getFirst_Name());
			model.setAge(employee.getAge());
			
			iEmployeeDao.save(model);
			
			response.setMessage("Data updated Successfully for Employee id: " + employeeid);
			response.setStatus("SUCCESS");
		}
		else
		{
			response.setMessage("ID not found/ there is no employee data to update");
			response.setStatus("FAILED");
			
		}
		
		return response;
	}
	
//	public String emailRegistration(Email email){
//		EmailRegistration emailRegistn = new EmailRegistration();
//		emailRegistn.setEmployee_id(email.());	
//	}
	
	
	
}
