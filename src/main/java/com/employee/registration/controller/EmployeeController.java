package com.employee.registration.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.employee.registration.dao.IEmployeeDao;
import com.employee.registration.dto.Employee;
import com.employee.registration.dto.RequestBean;
import com.employee.registration.dto.ResponseDto;
import com.employee.registration.service.IEmployeeService;
import com.google.gson.Gson;


@RestController
public class EmployeeController {

@Autowired
private IEmployeeDao iemployeeDao;

@Autowired
private IEmployeeService iEmployeeService; 
//*************** FOR READING AND PRINTING IT ON CONSOLE
/*@PostMapping("/save/employees")
public String saveEmployees(@RequestBody RequestBean requestbean) {

	iemployeeDao.saveEmployeeDetails();
	
	System.out.println(requestbean);
	
	
	return "Success";
}*/
@PostMapping("/save/employees")
public String saveEmployees(@RequestBody RequestBean requestbean) {

	System.out.println("from controller before section");

	iEmployeeService.saveAllEmployees(requestbean.getEmployees());
	System.out.println("from controller after section");

	return "Success";
}

@GetMapping("/employees/{employeeid}")
public ResponseDto fetchEmployeeDetails(@PathVariable Long employeeid) {
	ResponseDto response = iEmployeeService.fetchEmployeeDetails(employeeid);
/*Gson gson = new Gson();
String JsonString= gson.toJson(response);
	return JsonString;*/
	return response;
}

@PostMapping("/employee/update/{employeeid}")
public ResponseDto updateEmployees(@RequestBody Employee employee, @PathVariable Long employeeid)
{
	ResponseDto response = iEmployeeService.updateEmployeeDetails(employeeid, employee);
	return response;
}
}
