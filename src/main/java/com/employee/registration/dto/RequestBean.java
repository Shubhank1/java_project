package com.employee.registration.dto;

import java.util.List;

public class RequestBean {

	private List<Employee> employees;

	public List<Employee> getEmployees() {
		return employees;
	}

	public void setEmployees(List<Employee> employees) {
		this.employees = employees;
	}

	@Override
	public String toString() {
		return "RequestBean [employees=" + employees + "]";
	}
	
}
