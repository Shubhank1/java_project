package com.employee.registration.dto;

public class ResponseDto {
	private String message;
	private String status;
	private Employee employee;
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Employee getEmployee() {
		return employee;
	}
	public void setEmployee(Employee employee) {
		this.employee = employee;
	}
	@Override
	public String toString() {
		return "ResponseDto [message=" + message + ", status=" + status + ", employee=" + employee + "]";
	}
	
	
}
