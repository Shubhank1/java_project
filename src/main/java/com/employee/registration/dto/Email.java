package com.employee.registration.dto;

public class Email {

	private String type;
	private String email_id;
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getEmailId() {
		return email_id;
	}
	public void setEmailId(String emailId) {
		this.email_id = emailId;
	}
	@Override
	public String toString() {
		return "Email [type=" + type + ", emailId=" + email_id + "]";
	}
	
}
