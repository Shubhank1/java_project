package com.employee.registration.dto;


import java.util.List;
import com.employee.registration.dto.Email;
import com.employee.registration.dto.Address;
public class Employee {

	private String title;
	private String first_Name;
	private String middle_Name;
	private String last_Name;
	private Integer age;
	private String designation;
	private Boolean isActive;
	private Long primary_Phone;
	//private List<Email> emails;
	private String employeeType;
	//private Address permanent_Address;
	//private Address current_Address;
	
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getFirst_Name() {
		return first_Name;
	}

	public void setFirst_Name(String first_Name) {
		this.first_Name = first_Name;
	}

	public String getMiddle_Name() {
		return middle_Name;
	}

	public void setMiddle_Name(String middle_Name) {
		this.middle_Name = middle_Name;
	}

	public String getLast_Name() {
		return last_Name;
	}

	public void setLast_Name(String last_Name) {
		this.last_Name = last_Name;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Long getPrimary_Phone() {
		return primary_Phone;
	}

	public void setPrimary_Phone(Long primary_Phone) {
		this.primary_Phone = primary_Phone;
	}
	/*public Address getPermanent_Address() {
		return permanent_Address;
	}

	public void setPermanent_Address(Address permanent_Address) {
		this.permanent_Address = permanent_Address;
	}
	public List<Email> getEmails() {
		return emails;
	}

	public void setEmails(List<Email> emails) {
		this.emails = emails;
	}*/

	public String getEmployeeType() {
		return employeeType;
	}

	public void setEmployeeType(String employeeType) {
		this.employeeType = employeeType;
	}


	/*public Address getCurrent_Address() {
		return current_Address;
	}

	public void setCurrent_Address(Address current_Address) {
		this.current_Address = current_Address;
	}*/

	@Override
	public String toString() {
		return "Employee [title=" + title + ", first_Name=" + first_Name + ", middle_Name=" + middle_Name
				+ ", last_Name=" + last_Name + ", age=" + age + ", designation=" + designation + ", isActive="
				+ isActive + ", primary_Phone=" + primary_Phone + ", emails=" + ", employeeType="
				+ employeeType + ", permanent_Address=" + ", current_Address=" + "]";
	}
	
}