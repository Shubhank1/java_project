package com.employee.registration.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.employee.registration.model.EmployeeRegistrationModel;




@Repository
public interface IEmployeeDao extends CrudRepository<EmployeeRegistrationModel, Long>  {

	EmployeeRegistrationModel findOne(Long id);
	@SuppressWarnings("unchecked")
	EmployeeRegistrationModel save(EmployeeRegistrationModel employeeRegistrationModel);
	/*@Query(value = "INSERT INTO  FROM USERS WHERE EMAIL_ADDRESS = ?0", nativeQuery = true)
	  User findByEmailAddress(String emailAddress);*/
	}

