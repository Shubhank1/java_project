package com.employee.registration.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import com.employee.registration.model.EmployeeRegistrationModel;
@Entity
public class AddressRegistration {
		@Id
		private long id;
		public long getEmployee_id() {
			return employee_id;
		}
		public void setEmployee_id(long employee_id) {
			this.employee_id = employee_id;
		}
		public void setId(long id) {
			this.id = id;
		}
		private long employee_id;
	 private String line1;
	 private String line2;
	 private String city;
	 private String state;
	 private Integer postalcode;
	 private String addresstype;
	 	
	 	
	 	
	public String getAddresstype() {
		return addresstype;
	}
	public void setAddresstype(String addresstype) {
		this.addresstype = addresstype;
	}
		public String getLine1() {
			return line1;
		}
		public void setLine1(String line1) {
			this.line1 = line1;
		}
		public String getLine2() {
			return line2;
		}
		public void setLine2(String line2) {
			this.line2 = line2;
		}
		public String getCity() {
			return city;
		}
		public void setCity(String city) {
			this.city = city;
		}
		public String getState() {
			return state;
		}
		public void setState(String state) {
			this.state = state;
		}
		public Integer getPostalcode() {
			return postalcode;
		}
		public void setPostalcode(Integer postalcode) {
			this.postalcode = postalcode;
		}
		
}

