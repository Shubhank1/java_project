package com.employee.registration.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.ManyToAny;

@Entity
@Table(name = "EmailRegistration")
public class EmailRegistration {
	@Id
	private long id;
	private long employee_id;
	private String type;
	private String emailId;

	
public long getEmployee_id() {
		return employee_id;
	}

	public void setEmployee_id(long employee_id) {
		this.employee_id = employee_id;
	}

	public void setId(long id) {
		this.id = id;
	}

public String getType() {
	return type;
}

public void setType(String type) {
	this.type = type;
}

public String getEmailId() {
	return emailId;
}

public void setEmailId(String emailId) {
	this.emailId = emailId;
}


}
