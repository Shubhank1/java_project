package com.employee.registration.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "EmployeeRegistration")
public class EmployeeRegistrationModel {
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	private String title;
	private String first_Name;
	private String middle_Name;
	private String last_Name;
	private Integer age;
	private String designation;
	private Boolean isActive;
	private Long primary_Phone;
	private String employeeType;	
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getFirst_Name() {
		return first_Name;
	}

	public void setFirst_Name(String first_Name) {
		this.first_Name = first_Name;
	}

	public String getMiddle_Name() {
		return middle_Name;
	}

	public void setMiddle_Name(String middle_Name) {
		this.middle_Name = middle_Name;
	}

	public String getLast_Name() {
		return last_Name;
	}

	public void setLast_Name(String last_Name) {
		this.last_Name = last_Name;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Long getPrimary_Phone() {
		return primary_Phone;
	}

	public void setPrimary_Phone(Long primary_Phone) {
		this.primary_Phone = primary_Phone;
	}


	public String getEmployeeType() {
		return employeeType;
	}

	public void setEmployeeType(String employeeType) {
		this.employeeType = employeeType;
	}


	@Override
	public String toString() {
		return "EmployeeRegistrationModel [id=" + id + ", title=" + title + ", first_Name=" + first_Name
				+ ", middle_Name=" + middle_Name + ", last_Name=" + last_Name + ", age=" + age + ", designation="
				+ designation + ", isActive=" + isActive + ", primary_Phone=" + primary_Phone + ", employeeType="
				+ employeeType + ", emailRegistration=" + "]";
	}
	
}
